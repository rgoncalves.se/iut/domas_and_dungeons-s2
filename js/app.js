var databaseUrl = "http://rpg.dut-info.cf/rpg/";

var app = new Vue({
	el: '#dungeon',
	data: {
		players: players,
		player: players[0],
		items: items,
		itemCategories: itemCategories,
		effectTypes: effectTypes,
		selectedMode: 0,
		playersReady : false,
		itemsReady: false
	},
	template: `
	<div>\
	<div v-if="playersReady && itemsReady">\
		<div id="mode-area">\
			<label id="player-mode"> => Player</label>\
				<label id="dungeonMaster-mode">Dungeon Master <= </label>\
			<br>
			<input class="mode-selector" 
				type="range" min="0" max="1"
			v-bind:value="selectedMode"
			v-on:change="onModeChange($event)">\
		</div>\
		<div v-if="selectedMode == 0">
			<player-menu
			title="Get ready for Adventure !" 
			:players="this.players" 
			:player="this.player"
			:items="this.items"
			@onChangedPlayer = "updateCurrentPlayer">\
			</player-menu>\
		</div>\
		<div v-if="this.selectedMode == 1">
			<dungeon-master-menu
			title="Dungeon Master Manager"
			:items="this.items"
			:itemCategories="this.itemCategories"
			:effectTypes="this.effectTypes">\
			</dungeon-master-menu>
		</div>
	</div>\
	<div v-else>\
		<h2>Loading ...</h2>\
	</div>\
	</div>\
	`,
	methods: {
		onModeChange(e) {
			this.selectedMode = e.target.value;
		},
		updateCurrentPlayer(player) {
			this.player = player;
		},
		fetchItems() {}
	},
	mounted() {
		// get all items
		axios.get(databaseUrl + "items/get").then(response => {
			response.data.forEach(e => {
				this.items.push(Item.fromObject(e));
				//console.log(e);
			})
			this.itemsReady = true;
		});
		// get all players
		axios.get(databaseUrl + "persos/get").then(response => {
			response.data.forEach(e => {
				this.players.push(Player.fromObject(e));
				//console.log(e);
			})
			this.player = this.players[0];
			this.playersReady = true;
		});
	}
})
