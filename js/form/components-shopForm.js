Vue.component("shopForm", {
	props : ["title", "itemsList", "player"],
	data : function() {
		return {
			nbDisplayed : 10,
			chosenItemId : null,
			chosenItem : null,
			name : "defaultName",
			proposedItems : [],
		}
	},
	template : 
	`<div>\
		<h3>{{title}}</h3>\
		<label>Max. Items</label>
		<input class="orange" type="number" v-model="nbDisplayed">\

		<label>Item's Price : </label>\
		<input readonly v-bind:value="chosenItem.price" v-if="chosenItemId != null">\
		<input readonly v-bind:value="0" v-if="chosenItemId == null">\

		<select class="selection" 
		v-model="chosenItemId"
		@change="onSelectedItem($event)"
		:key="nbDisplayed">\
			<option v-for="(item, i) in proposedItems"
			v-if="i < nbDisplayed" 
			v-bind:value="item._id">
			{{item.name}}</option>\
		</select>\
		<div class="rpgui-center">
		<button type="button" class="rpgui-button" v-on:click="buyAndUpdate(chosenItemId)">BUY ITEM</button>\
		</div>
	</div>\
	`,
	methods : {
		onSelectedItem(event) 
		{
			console.log(event.target.value);
			this.chosenItemId = event.target.value;
			this.chosenItem = items.find(i => i._id === this.chosenItemId);
		},
		buyAndUpdate()
		{
			var msg = "Are you sure to buy this item : \n" + 
				" - " + this.chosenItem.name +
				" for " + this.chosenItem.price;
			if(this.player.gold > this.chosenItem.price) {
				if(confirm(msg)) {
					this.player.buy(this.chosenItem);
					this.proposedItems.splice(this.proposedItems.indexOf(this.chosenItem), 1);
					if(this.proposedItems.length == 0) {
						this.initContent();
					}
				}
			} else {
				alert("Not enough money ! ");
			}
		},
		initContent()
		{
			console.log("Creating new random shop");
			var length = getRandomInt(16);
			for(var i = 0; i < length; i++) {
				this.proposedItems.push(this.itemsList[getRandomInt(this.itemsList.length - 1)]);
			};
		}
	},
	mounted() {
		this.initContent();
	}
});
