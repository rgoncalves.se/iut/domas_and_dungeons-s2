Vue.component("itemCreationForm", {
	data : function() {
		return {
			name : "",
			category : "",
			price : 20,
			effectType : "",
			effectModifier : 10
		}
	},
	props : ["title", "items", "itemCategories", "effectTypes"],
	template : 
	`<div>\
		<h3>{{title}}</h3>\
		<ul class="editable-properties">\
			<li> _NAME______:
			<input v-model="name" 
			v-on:change="updateValue($event, this.name)">
			</li>\
			<li> _CATEGORY__:
				<select class="selection" v-model="category">\
					<option v-for="category in itemCategories"
					v-bind:value="category"
					v-on:change="updateValue($event, this.category)">
					{{category}}
					</option>\
				</select>\
			</li>\
			<li> _PRICE_____:
				<input type="number" step="1" 
				v-model="price" 
				v-on:change="updateValue($event, this.price)">\
			</li>\
			<li> _EFFECT____:
				<select class="selection effect-input" v-model="effectType">\
					<option v-for="effect in effectTypes"
					v-bind:value="effect.mod"
					v-on:change="updateValue($event, this.effectType)">
					{{effect.name}}
					</option>\
				</select>\
				<input class="effect-input" 
				type="number" step="1" 
				v-model="effectModifier"
				v-on:change="updateValue($event, effectModifier)">\
			</li>\
		</ul>\
		<div class="rpgui-center">
		<button type="button" class="rpgui-button" v-on:click="createItem">CREATE ITEM</button>\
		</div>
	</div>\
	`,
	methods : {
		createItem()
		{
			if(this.effectModifier > -1) {
				this.effectModifier = "+" + this.effectModifier;
			}
			items.push(new Item(items.length, this.name, this.category, this.price, this.effectType + "" + this.effectModifier));
			this.postItemCreation();
			console.log(items[items.length - 1 ]);
			alert("Item created : " + this.name
				+ "\n - category : " + this.category
				+ "\n - price    : " + this.price
				+ "\n - effect   : " + this.effectType + "" + this.effectModifier);
		},
		updateValue(e, val)
		{
			val = e.target.value;
		},
		postItemCreation()
		{
			console.log("Post created items");
			axios.post(databaseUrl + "items/create", 
			{
				"name" : this.name,
				"type" : this.category,
				"price" : parseInt(this.price),
				"effect" : this.effectType + "" + this.effectModifier
			}).then(response => {
				console.log(response.data);
			});
		}
	},
	watch : {
	}
});
