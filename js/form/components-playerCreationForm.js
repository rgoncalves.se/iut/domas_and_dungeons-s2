Vue.component("playerCreationForm", {
	data : function() {
		return {
			name : "",
			gold : 10,
			createdPlayer : null
		}
	},
	props : ["title", "players"],
	template : 
	`<div>\
		<h3>{{title}}</h3>\
		<ul class="editable-properties">\
			<li> _NAME______:
			<input v-model="name" 
			v-on:change="updateValue($event, this.name)">
			</li>\
			<li> GOLD_______:
				<input type="number" step="1" 
				v-model="gold" 
				v-on:change="updateValue($event, this.gold)">\
			</li>\
		</ul>\
		<div class="rpgui-center">
		<button type="button" class="rpgui-button" v-on:click="createPlayer">CREATE PLAYER</button>\
		</div>
	</div>\
	`,
	methods : {
		createPlayer : function()
		{
			this.createdPlayer = new Player("", this.name, 1);
			players.push(this.createdPlayer);
			this.createdPlayer.gold = this.gold;
			console.log(this.createdPlayer);
			this.postPlayerCreation();
			alert("Player created : " + this.name
				+ "\n - gold : " + this.gold);
		},
		updateValue : function(e, val)
		{
			val = e.target.value;
		},
		postPlayerCreation(slot, items)
		{
			console.log("Post created player");
			axios.post(databaseUrl + "persos/create", 
			{
				"name" : this.createdPlayer.name,
				"level" : parseInt(this.createdPlayer.level),
				"gold" : parseInt(this.createdPlayer.gold),
				"life" : parseInt(this.createdPlayer.life),
				"vitality" : parseInt(this.createdPlayer.vitality),
				"strength" : parseInt(this.createdPlayer.strength),
				"armor" : parseInt(this.createdPlayer.armor),
			}).then(response => {
				console.log(response.data);
			});
		}

	},
	watch : {
	}
});
