Vue.component("playerForm", {
	props : ["title", "player", "players"],
	data : function() {
		return {
			selectedPlayerId : 0,
			idDrag : null,
			cheatsActivated : false
		}
	},
	template :
	`
	<div>\
	<h3>{{title}}</h3>\
	<div class="player-selection">\
		<select class="selection" @change="onChangedPlayer($event)">\
			<option v-for="(player,i) in players"
			v-bind:value="i">
			{{i}} :: {{player.name}}
			</option>\
		</select>\
		<input class="cheats-activator" type="checkbox" v-model="cheatsActivated"><label>Cheats</label>
	</div>\
	<ul class="editable-properties">\
		<li> _NAME______:
			<input :readonly="!this.cheatsActivated" v-model="player.name">
		</li>\
		<li> _LEVEL_____:
			<input :readonly="!this.cheatsActivated" v-model="player.level">
		</li>\
		<li> _LIFE______:
		<input :readonly="!this.cheatsActivated" v-model="player.life">
		</li>\
		<li> _STRENGTH__:
			<input :readonly="!this.cheatsActivated" v-model="player.strength">
		</li>\
		<li> _VITALITY__: 
			<input :readonly="!this.cheatsActivated" v-model="player.vitality">
		</li>\
		<li> _ARMOR_____:
			<input :readonly="!this.cheatsActivated" v-model="player.armor">
		</li>\
		<li> _GOLD______:
			<input :readonly="!this.cheatsActivated" v-model="player.gold">
		</li>\
	</ul>\
	<table>\
	<tr>\
	<th>Slots</th>\
	<th>Items</th>\
	</tr>\
	<tr v-for="slot in player.slots">\
	<td draggable=true @dragover.prevent @drop="dropItem(slot.name)"><strong>{{slot.name}}</strong></td>\
	<td>\
	<button class="rpgui-button" v-for="item in slot.items">{{item.name}}</button>\
	</td>\
	</tr>\
	<tr>\
	<td><strong>Bought items</strong></td>\
	<td>\
	<button class="rpgui-button" draggable=true v-for="(item, index) in player.boughtItems" @dragstart="dragItemStart(index)" @dragend="dragItemEnd()">{{item.name}}</button>\
	</td>\
	</tr>\
	</table>\

	</div>\
	`,
	methods : {
		onChangedPlayer(event) 
		{
			this.selectedPlayerId = event.target.value;
			this.$emit("onChangedPlayer", players[this.selectedPlayerId]);
		},
		dragItemStart(index)
		{
			console.log("start dragging " + this.player.boughtItems[index].name);
			this.idDrag = index;
		},
		dragItemEnd()
		{
			console.log("stop dragging");
			idDrag = -1;
		},
		dropItem(slot)
		{  
			console.log("item dropped on " + slot);
			this.player.assign(this.idDrag, slot);
			var existingItems = this.player.getAllItemsFromSlot(slot);
			console.log(existingItems)
			this.putPlayerUpdate(slot, existingItems);
		},
		putPlayerUpdate(slot, items)
		{
			console.log("Update player items");
			axios.put(databaseUrl + "persos/updateslot", 
			{
				"id" : this.player._id,
				"slotName" : slot,
				"items" : items
			}).then(response => {
				console.log(response.data);
			});
		}
	},
	watch : {
		cheatsActivated : function(val) 
		{
			if(val) {
				console.log(" - Cheats activated");
			}
			else {
				console.log(" - Cheats deactivated");
			}
		}
	}
});
