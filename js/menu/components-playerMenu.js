Vue.component("playerMenu", {
	data : function() {
		return {
		}
	},
	props : ["title", "items", "player", "players"],
	template : 
	`<div>\
		<hr>\
		<h2>{{title}}</h2>\
		<hr>
		<player-form
		title="Player"
		:players="players"
		:player="player"
		@onChangedPlayer = "updateCurrentPlayer">\
		</player-form>
		<hr>
		<shop-form
		title="Shop"
		:player="player"
		:itemsList="items">\
		</shop-form>
	</div>\
	`,
	methods : {
		updateCurrentPlayer(player)
		{
			this.$emit("onChangedPlayer", players[event.target.value]);
		}
	},
	watch : {
	}
});
