Vue.component("dungeonMasterMenu", {
	data : function() {
		return {
		}
	},
	props : ["title", "items", "itemCategories", "effectTypes", "players"],
	template : 
	`<div>\
		<hr>\
		<h2>{{title}}</h2>\
		<hr>
		<item-creation-form
		title="Item Creator"
		:items="items"
		:itemCategories="itemCategories"
		:effectTypes="effectTypes">\
		</item-creation-form>
		<hr>
		<player-creation-form
		title="Player Creator"
		:players="players">\
		</player-creation-form>
	</div>\
	`,
	methods : {
	},
	watch : {
	}
});
